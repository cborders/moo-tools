package productions.moo.minecraft.moo_tools;

import net.minecraft.block.BlockState;
import net.minecraft.block.FacingBlock;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.ActionResult;
import net.minecraft.util.BlockRotation;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

import java.util.Arrays;
import java.util.List;

public class TurningWrench extends Item {
    private static final List<String> HORIZONTAL_TWIST = Arrays.stream(new String[] {
            "minecraft:repeater",
            "minecraft:comparator",
            "minecraft:hopper",
            "minecraft:white_glazed_terracotta",
            "minecraft:white_glazed_terracotta",
            "minecraft:orange_glazed_terracotta",
            "minecraft:orange_glazed_terracotta",
            "minecraft:magenta_glazed_terracotta",
            "minecraft:magenta_glazed_terracotta",
            "minecraft:light_blue_glazed_terracotta",
            "minecraft:light_blue_glazed_terracotta",
            "minecraft:yellow_glazed_terracotta",
            "minecraft:yellow_glazed_terracotta",
            "minecraft:lime_glazed_terracotta",
            "minecraft:lime_glazed_terracotta",
            "minecraft:pink_glazed_terracotta",
            "minecraft:pink_glazed_terracotta",
            "minecraft:gray_glazed_terracotta",
            "minecraft:gray_glazed_terracotta",
            "minecraft:black_glazed_terracotta",
            "minecraft:black_glazed_terracotta",
            "minecraft:red_glazed_terracotta",
            "minecraft:red_glazed_terracotta",
            "minecraft:green_glazed_terracotta",
            "minecraft:green_glazed_terracotta",
            "minecraft:brown_glazed_terracotta",
            "minecraft:brown_glazed_terracotta",
            "minecraft:blue_glazed_terracotta",
            "minecraft:blue_glazed_terracotta",
            "minecraft:purple_glazed_terracotta",
            "minecraft:purple_glazed_terracotta",
            "minecraft:cyan_glazed_terracotta",
            "minecraft:cyan_glazed_terracotta",
            "minecraft:light_gray_glazed_terracotta",
            "minecraft:light_gray_glazed_terracotta",
    }).toList();

    private static final List<String> FULL_TWIST = Arrays.stream(new String[] {
            "minecraft:piston",
            "minecraft:sticky_piston",
            "minecraft:dropper",
            "minecraft:dispenser",
            "minecraft:observer",
    }).toList();

    public TurningWrench(Settings settings) {
        super(settings);
    }

    @Override
    public TypedActionResult use(World world, PlayerEntity player, Hand hand) {
        return TypedActionResult.success(player.getStackInHand(hand));
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        PlayerEntity player = context.getPlayer();
        player.playSound(SoundEvents.BLOCK_BONE_BLOCK_HIT, 1.0f, 1.0f);

        BlockState state = context.getWorld().getBlockState(context.getBlockPos());
        String blockName = Registries.BLOCK.getId(state.getBlock()).toString();

        if (!HORIZONTAL_TWIST.contains(blockName) && !FULL_TWIST.contains(blockName)) {
            return ActionResult.success(true);
        }

        World world = context.getWorld();


        if (HORIZONTAL_TWIST.contains(blockName)) {
            state = state.rotate(BlockRotation.CLOCKWISE_90);
        } else {
            state = state.with(FacingBlock.FACING, getNextFacing(state));
        }

        world.setBlockState(context.getBlockPos(), state);
        state.neighborUpdate(world, context.getBlockPos(), state.getBlock(), context.getBlockPos(), true);

        return ActionResult.success(true);
    }

    private Direction getNextFacing(BlockState state) {
        return switch (state.get(FacingBlock.FACING)) {
            case DOWN -> Direction.NORTH;
            case UP -> Direction.DOWN;
            case NORTH -> Direction.EAST;
            case SOUTH -> Direction.WEST;
            case WEST -> Direction.UP;
            case EAST -> Direction.SOUTH;
        };
    }
}

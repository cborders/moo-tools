package productions.moo.minecraft.moo_tools;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.registry.Registries;
import net.minecraft.registry.Registry;
import net.minecraft.util.Identifier;

public class MooServerTools implements ModInitializer {
    private static final String MOD_ID = "moo_tools";
    private static final Item WRENCH = new TurningWrench(new Item.Settings());

    @Override
    public void onInitialize() {
        Registry.register(Registries.ITEM, Identifier.of(MOD_ID, "turning_wrench"), WRENCH);
    }
}
